function makeTable() {
    let form = document.frm;
    let rows = parseInt(form.rows.value);
    let columns = parseInt(form.columns.value);

    if(isNaN(rows) || isNaN(columns) || rows < 1 || columns < 1) {
        alert("Incorrect arguments");
    } else {
        let table = document.createElement("table");
        table.id = "table";
    
        for(let i = 1; i < rows + 1; i++){
            let elem = document.createElement("tr");
            for(let j = 1; j < columns + 1; j++){
                let cell = document.createElement("td");
                cell.addEventListener("click", changeColor);
                elem.appendChild(cell);
            }
            table.appendChild(elem);
        }
    
        document.body.appendChild(table);
    }


}

function tableExists() {
    let table = document.getElementById("table");
    let form = document.frm;
    let rows = parseInt(form.rows.value);
    let columns = parseInt(form.columns.value);
    
    if(table !== null && !isNaN(rows) && !isNaN(columns)) {
        let cells = table.getElementsByTagName("td");
        
        for(let cell of cells) {
            cell.removeEventListener("click", changeColor);
        }

        document.body.removeChild(table);
    }
}

function fillTable() {
    let form = document.frm;
    let rows = parseInt(form.rows.value);
    let columns = parseInt(form.columns.value);

    if (!isNaN(rows) && !isNaN(columns)) {
        let tble = document.getElementById("table");
        let rowArray = tble.getElementsByTagName("tr");

        for(let i = 1; i < rowArray.length + 1; i++){
            let columnArray = rowArray[i-1].getElementsByTagName("td");

            for(let j = 1; j < columnArray.length + 1; j++){
                let text = document.createTextNode(i.toString()+j.toString());
                columnArray[j-1].appendChild(text);
            }
        }
    }
}

function changeColor(e) {
    e.target.classList.toggle("clickedCell");
}

let run = document.getElementById("run");

run.addEventListener("click", tableExists);
run.addEventListener("click", makeTable);
run.addEventListener("click", fillTable);
